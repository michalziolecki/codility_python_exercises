from datetime import datetime


def filter_users(files_str: str) -> list:
    admin_files = []
    files: list = files_str.split('\n')
    for file in files:
        if not file:
            continue
        file_data = file.split(' ')
        if file_data:
            if file_data[0] == 'admin':
                admin_files.append(file)

    return admin_files


def filter_permissions(files: list):
    executed_files = []
    for file in files:
        if not file:
            continue
        file_data = file.split(' ')
        file_data = clear_data_of_file(file_data)
        try:
            permissions = file_data[1]
            if 'x' in permissions:
                executed_files.append(file)
        except AttributeError as ae:
            continue

    return executed_files


def clear_data_of_file(data_l: list) -> list:
    clean_data = []
    for data in data_l:
        if data:
            clean_data.append(data)
    return clean_data


def filter_size(files: list):
    size_filtered_files = []
    for file in files:
        if not file:
            continue
        file_data = file.split(' ')
        file_data = clear_data_of_file(file_data)
        try:
            size = file_data[5]
            if int(size) < 14 * (2 ** 20):
                size_filtered_files.append(file)
        except (AttributeError, ValueError):
            continue
    return size_filtered_files


def parse_date(files: list) -> list:
    date_list = []
    for file in files:
        if not file:
            continue
        file_data = file.split(' ')
        file_data = clear_data_of_file(file_data)
        try:
            day = int(file_data[2])
            month = int(short_month_to_int(file_data[3]))
            year = int(file_data[4])
            parsed_date = datetime(year, month, day)
            date_list.append(parsed_date)
        except (AttributeError, ValueError):
            continue

    return date_list


def short_month_to_int(shortcut: str):
    month = {
        'Jan': 1,
        'Feb': 2,
        'Mar': 3,
        'Apr': 4,
        'May': 5,
        'Jun': 6,
        'Jul': 7,
        'Aug': 8,
        'Sep': 9,
        'Oct': 10,
        'Nov': 11,
        'Dec': 12,
    }
    return month.get(shortcut, '')


def filter_date(date_list: list) -> str:
    lowest_date_str = ''
    if date_list:
        lowest_date = date_list[0]
        for _date in date_list:
            if lowest_date > _date:
                lowest_date = _date
        if lowest_date:
            lowest_date_str = lowest_date.strftime("%d %b %Y")

    return lowest_date_str


def solution(S):
    result = "NO FILES"
    admin_files: list = filter_users(S)
    exec_files: list = filter_permissions(admin_files)
    filtered_files: list = filter_size(exec_files)
    date_list: list = parse_date(filtered_files)
    lowest_date: str = filter_date(date_list)
    if lowest_date:
        result = lowest_date
    return result


def main():
    test = """
admin  -wx 29 Sep 1983        833 source.h
admin  r-x 23 Jun 2003     854016 blockbuster.mpeg
admin  --x 02 Jul 1997        821 delete-this.py
admin  -w- 15 Feb 1971      23552 library.dll
admin  --x 15 May 1979  645922816 logs.zip
jane   --x 04 Dec 2010      93184 old-photos.rar
jane   -w- 08 Feb 1982  681574400 important.java
admin  rwx 26 Dec 1952   14680064 to-do-list.txt
"""
    print(solution(test))


if __name__ == "__main__":
    main()
