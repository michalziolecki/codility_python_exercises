def find_positive(sort_tab: []) -> []:
    positive_tab = []
    for numb in sort_tab:
        if numb > 0:
            positive_tab.append(numb)
    sorted_tab = sorted(positive_tab)
    return sorted_tab


def find_lowest(positive_tab: []) -> int:
    min_val = 0
    if len(positive_tab) <= 0:
        return 1
    idx = 1
    last = 0
    stop = False
    for numb in positive_tab:
        if last == numb:
            continue
        if idx != numb:
            min_val = idx
            stop = True
        idx += 1
        last = numb
        if stop:
            break
    if min_val == 0:
        min_val = last + 1
    return min_val


def solution(A):
    positive_tab = find_positive(A)
    min_val = find_lowest(positive_tab)
    return min_val


def main():
    test = [-2, -5, 2, 3, 1, 8]
    print(solution(test))


if __name__ == "__main__":
    main()
