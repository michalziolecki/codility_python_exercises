from datetime import datetime


class NameLinker:

    def __init__(self, pos, old_name):
        self.__pos = pos
        self.old_name = old_name
        self.data = []
        self.new_name = ''
        self.group = ''

    @property
    def pos(self):
        return self.__pos

    @pos.setter
    def pos(self, pos):
        self.__pos = pos


def rename_files(input_data: str) -> str:
    name_liner_list = []
    files = parse_input(input_data)
    for idx, file in enumerate(files):
        name_liner_list.append(NameLinker(idx, file))
    files_data_list = parse_photo_data(files, name_liner_list)
    grouped_files: dict = group_photos(files_data_list, name_liner_list)
    grouped_sorted_files: dict = sort_by_date(grouped_files)
    named_files_in_group(grouped_sorted_files)

    return build_str_response(name_liner_list)


def parse_input(input_data: str) -> list:
    files = []
    for file in input_data.split('\n'):
        if file:
            files.append(file)
    return files


def parse_photo_data(files: list, name_liner_list: list):
    data_files_list = []
    for file in files:
        data = file.split(', ')
        for link in name_liner_list:
            link: NameLinker
            if link.old_name == file:
                link.data = data
        data_files_list.append(data)
    return data_files_list


def group_photos(files_data: list, name_liner_list: list):
    groups = {}
    for file_data in files_data:
        city = ''
        try:
            city = file_data[1]
        except IndexError as ie:
            # print(f'exception: {ie.args}')
            continue
        if groups.get(city, None):
            for link in name_liner_list:
                if link.data == file_data:
                    link.group = city
                    groups.get(city).append(link)
        else:
            for link in name_liner_list:
                if link.data == file_data:
                    link.group = city
                    groups[city] = [link]
    return groups


def sort_by_date(grouped_files_data: dict) -> dict:
    sorted_grouped_files = {}
    date_list = []
    for group in grouped_files_data.keys():
        date_file_dict = {}
        linker_list = grouped_files_data.get(group)
        for linker in linker_list:
            datetime_val = datetime.strptime(linker.data[2], '%Y-%m-%d %H:%M:%S')
            date_list.append(datetime_val)
            date_file_dict[linker.data[2]] = linker
        sorted_date_list = sorted(date_list)
        for _date in sorted_date_list:
            date_str: str = datetime.strftime(_date, '%Y-%m-%d %H:%M:%S')
            linker = date_file_dict.get(date_str, None)
            if linker:
                # print(vars(linker))
                if sorted_grouped_files.get(group, None):
                    sorted_grouped_files.get(group).append(linker)
                else:
                    sorted_grouped_files[group] = [linker]

    return sorted_grouped_files


def named_files_in_group(grouped_files_data: dict):
    def add_leading_zero(value: int, group: int) -> str:
        value_str = ''
        value_len = len(str(value))
        max_len = len(str(group))
        lead_zero_numb = max_len - value_len
        if lead_zero_numb == 0:
            return str(value)
        for idx in range(0, lead_zero_numb):
            if not value_str:
                value_str = f'0{value}'
            else:
                value_str = f'0{value_str}'
        return value_str

    def get_extension(file_name: str) -> str:
        dot_pos = file_name.rfind('.')
        return file_name[dot_pos + 1:]

    for group_name in grouped_files_data.keys():
        linkers: list = grouped_files_data.get(group_name)
        group_size: int = len(linkers)
        for idx, linker in enumerate(linkers, start=1):
            if linker.data:
                linker.new_name = f'{group_name}{add_leading_zero(idx, group_size)}.{get_extension(linker.data[0])}'


def build_str_response(linker_list: list) -> str:
    names = [linker.new_name for linker in linker_list]
    return '\n'.join(names)


def solution(S):
    result = rename_files(S)
    return result


def main():
    test = """
photo.jpg, Warsaw, 2013-09-05 14:08:15
john.png, London, 2015-06-20 15:13:22
myFriends.png, Warsaw, 2013-09-05 14:07:13
Eiffel.jpg, Paris, 2015-07-23 08:03:02
pisatower.jpg, Paris, 2015-07-22 23:59:59
BOB.jpg, London, 2015-08-05 00:02:03
notredame.png, Paris, 2015-09-01 12:00:00
me.jpg, Warsaw, 2013-09-06 15:40:22
a.png, Warsaw, 2016-02-13 13:33:50
b.jpg, Warsaw, 2016-01-02 15:12:22
c.jpg, Warsaw, 2016-01-02 14:34:30
d.jpg, Warsaw, 2016-01-02 15:15:01
e.png, Warsaw, 2016-01-02 09:49:09
f.png, Warsaw, 2016-01-02 10:55:32
g.jpg, Warsaw, 2016-02-29 22:13:11
"""
    print(solution(test))


if __name__ == "__main__":
    main()
