def calculate_stack(params: str, stack: []) -> int:
    def duplicate():
        value = stack.pop()
        stack.append(value)
        stack.append(value)

    def stack_pop():
        stack.pop()

    def sum_topmost():
        value_one = stack.pop()
        value_two = stack.pop()
        sum_val = value_one + value_two
        check_limit_numb(sum_val)
        stack.append(sum_val)

    def subtract_topmost():
        value_one = stack.pop()
        value_two = stack.pop()
        sub_val = value_one - value_two
        check_limit_numb(sub_val)
        stack.append(sub_val)

    operations = {
        'DUP': duplicate,
        'POP': stack_pop,
        '+': sum_topmost,
        '-': subtract_topmost
    }

    params: list = parse_input(params)
    if not params:
        return -1
    # print(f'params {params}')
    for param in params:
        if is_integer(param):
            stack.append(int(param))
            # print(f'added {param}')
            continue
        operations[param]()

    return stack.pop()


def parse_input(input_values: str) -> list:
    operations = input_values.split(' ')
    return operations


def is_integer(n):
    try:
        float(n)
    except ValueError:
        return False
    else:
        return float(n).is_integer()


def check_limit_numb(value: int):
    if value < 0 or value > ((2 ** 20) - 1):
        raise ValueError


def solution(S):
    stack = []
    try:
        result = calculate_stack(S, stack)
    except:
        # print('exception')
        return -1
    return result


def main():
    test = "13 DUP 4 POP 5 DUP + DUP + -"
    # test = "5 6 + -"
    # test = "3 DUP 5 - -"
    print(solution(test))


if __name__ == "__main__":
    main()
